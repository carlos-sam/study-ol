import { Tile as TileLayer, Vector as VectorLayer } from "ol/layer";
import { Vector as VectorSource } from "ol/source";
import Feature from "ol/feature";
import { Point, LineString, Polygon } from "ol/geom";
import {
  Style,
  Stroke,
  Fill,
  Circle as CircleStyle,
  Icon,
  Text,
} from "ol/style";

// 绘制辐射圈
/**
 *
 * @param {*} map  地图实例
 * @param {Array} data  辐射圈数据  格式为 [{ id: 1, "coordinate": [114.2825, 30.5931] }]
 * @param {number} radius 辐射圈半径(以像素为单位)
 * @param {string} color 辐射圈颜色
 * @param {number} rippleNumber 辐射圈波动次数 [1-5]
 * @param {number} speed 辐射圈波动速度 [0-1]
 */
export const drawRadiationCircle = (
  map,
  data,
  radius = 20,
  color,
  rippleNumber = 3,
  speed = 0.5
) => {
  let layer = new VectorLayer({
    properties: {
      name: "辐射圈图层",
      id: "RadiationCircle",
    },
    source: new VectorSource(),
  });

  const getRadiationStyle = (radiusQueue, color = "rgba(255, 0, 0, 0.8)") => {
    return [
      ...radiusQueue.map((radius, index) => {
        return new Style({
          image: new CircleStyle({
            radius,

            stroke: new Stroke({
              color,
              width: 5,
            }),
          }),
        });
      }),
    ];
  };

  let radiationCircleUtils = {
    // 获取辐射圈相关参数
    get(key) {
      return this[key];
    },
    // 设置辐射圈相关参数
    set(key, value) {
      if (key === "speed") {
        speed = (radius * value) / 100;
      }
      this[key] = value;
    },
    // 获取辐射圈图层
    getLayer() {
      return layer;
    },
    // 清除所有辐射圈
    clear() {
      layer.getSource().clear();
    },
    // 添加辐射圈
    add(items) {
      const features = items.map(item => {
        const feature = new Feature({
          geometry: new Point(item.coordinate),
        });
        feature.setId(item.id);
        return feature;
      });
      layer.getSource().addFeatures(features);
    },
    // 移除指定id的辐射圈
    remove(id) {
      layer.getSource().removeFeature(layer.getSource().getFeatureById(id));
    },
    // 销毁辐射圈图层
    destroy() {
      map.removeLayer(layer);
      layer = null;
      radiationCircleUtils = null;
    },
  };

  // 保存·辐射圈相关参数·
  radiationCircleUtils.set("speed", speed);
  radiationCircleUtils.set("radius", radius);
  radiationCircleUtils.set("color", color);
  radiationCircleUtils.set("rippleNumber", rippleNumber);
  // 给图层添加数据
  radiationCircleUtils.add(data);

  // 辐射圈队列
  const queue = [0];
  // 给图层设置样式
  layer.setStyle(getRadiationStyle(queue, radiationCircleUtils.color));

  // 修改图层样式制造辐射圈效果
  layer.on("postrender", () => {
    if (queue[0] >= radius) {
      queue.shift();
    }

    if (
      queue[queue.length - 1] >=
        radiationCircleUtils.radius / radiationCircleUtils.rippleNumber &&
      queue.length < radiationCircleUtils.rippleNumber
    ) {
      queue.push(0);
    }

    queue.forEach((el, index) => {
      queue[index] += radiationCircleUtils.speed;
    });

    layer.setStyle(getRadiationStyle(queue, radiationCircleUtils.color));
  });

  map.addLayer(layer);

  return radiationCircleUtils;
};
