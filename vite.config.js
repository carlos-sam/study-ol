import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import raw from "vite-plugin-raw";
import path from "path";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    raw({
      match: /\.geojson$/,
    }),
  ],
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "./src"),
    },
  },
});
